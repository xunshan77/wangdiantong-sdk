wangdiantong-sdk for PHP
----

* 运行最底要求 PHP 版本 5.4 , 建议在 PHP7 上运行以获取最佳性能；
* 接口需要缓存数据在本地，因此对目录需要有写权限；
* 我们鼓励大家使用 composer 来管理您的第三方库，方便后期更新操作；

代码仓库
----
wangdiantong-sdk 为开源项目，允许把它用于任何地方，不受任何约束，欢迎 fork 项目。

* Gitee 托管地址：https://gitee.com/xunshan77/wangdiantong-sdk

安装使用
----
1.1 通过 Composer 来管理安装

```shell
# 首次安装 线上版本（稳定）
composer require 77xunshan/wangdiantong-sdk

# 首次安装 开发版本（开发）
composer require 77xunshan/wangdiantong-sdk dev-master

# 更新
composer update 77xunshan/wangdiantong-sdk
```

2.1 接口实例所需参数

```php
$publicPrams = [
    'sid' => 'your sid',
    'appkey' => 'your appkey',
    'appsecret' => 'your appsecret',
];
```

3.1 实例指定接口

```php
try {
    $base = Base::instance($publicPrams, true);
    //接口专用参数
    $result = $base->shop([
    ]);
    var_dump($result);
} catch (\Exception $e) {
    throw $e;
}
```

* 更多功能请阅读测试代码或SDK封装源码

开源协议
----

* wangdiantong-sdk 基于`MIT`协议发布，任何人可以用在任何地方，不受约束
* wangdiantong-sdk 部分代码来自互联网，若有异议，可以联系作者进行删除
