<?php

namespace wangdian\exception;

class InvalidFileSystemException extends \Exception
{
    /**
     * @var array
     */
    public $raw = [];
    /**
     * InvalidResponseException constructor.
     * @param string $message
     * @param integer $code
     * @param array $raw
     */
    public function __construct($message, $code = 0, $raw = [])
    {
        parent::__construct($message, intval($code));
        $this->raw = $raw;
    }
}
