<?php

namespace wangdian\sdk;

use wangdian\base\BaseApi;
use wangdian\exception\InvalidConfigException;
use wangdian\exception\InvalidFileSystemException;
use wangdian\exception\InvalidResponseException;

/**
 * Class Service
 * 售后类
 *
 * @package wangdian\sdk
 */
class Service extends BaseApi
{
    /**
     * 创建原始退款单
     *
     * @desc 销售订单（包括发货前和发货后）退款或退货单据推送至ERP。
     * 注：销售订单的售后换货订单此接口推送“退货类型”退款单，换出订单“trade_push.php”推送
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=sales_refund_push.php
     */
    public function salesRefundPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/sales_refund_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/sales_refund_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建退货入库单
     *
     * @desc 推送ERP销售退货（换货）订单对应的入库单据给ERP，推送前提ERP的退换单状态为“待收货”
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_refund_push.php
     */
    public function stockinRefundPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_refund_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_refund_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询退换管理
     *
     * @desc 获取ERP销售退货（换货）订单信息
     * 注：本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=refund_query.php
     */
    public function refundQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/refund_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/refund_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询退货入库单
     *
     * @desc 获取ERP销售退货（换货）订单对应的入库单信息
     * 注：本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_order_query_refund.php
     */
    public function stockinOrderQueryRefund($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_order_query_refund.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_order_query_refund.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询销售退货汇总
     *
     * @desc 获取销售订单退回货品汇总数据，查询EREP统计→销售→单品销售汇总表的“退货量”。
     * 注：本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_stat_refund_by_spec_shop_warehouse_query.php
     */
    public function vipStatRefundBySpecShopWarehouseQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_stat_refund_by_spec_shop_warehouse_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_stat_refund_by_spec_shop_warehouse_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询原始退款单
     *
     * @desc 获取ERP的原始退款单信息
     * 注：本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_api_refund_query.php
     */
    public function vipApiRefundQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_api_refund_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_api_refund_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }
}
