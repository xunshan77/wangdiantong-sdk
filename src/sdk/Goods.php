<?php

namespace wangdian\sdk;

use wangdian\base\BaseApi;
use wangdian\exception\InvalidConfigException;
use wangdian\exception\InvalidFileSystemException;
use wangdian\exception\InvalidResponseException;

/**
 * Class Goods
 * 货品类
 *
 * @package wangdian\sdk
 */
class Goods extends BaseApi
{
    /**
     * 创建货品档案
     *
     * @desc ①批量推送货品资料给ERP
     * ②批量更新ERP货品档案资料（支持已经存在的货品（spu）
     * ，新增单品（sku），good_list中good_no确定sku新增在哪个货品之下，
     * 将新增的spec_no以及对应信息放在spec_list，
     * 推送成功以后，sku将新增到对应的货品档案（spu）下。）
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=goods_push.php
     */
    public function goodsPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/goods_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/goods_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询货品档案
     *
     * @desc 获取ERP的货品档案资料，“货品档案”其他系统称为“物料档案”“商品档案”等
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=goods_query.php
     */
    public function goodsQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/goods_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/goods_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建平台货品
     *
     * @desc 在推送销售订单给ERP之前，需要映射匹配ERP系统单品或者设置同步库存的策略时调用
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=api_goodsspec_push.php
     */
    public function apiGoodsspecPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/api_goodsspec_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/api_goodsspec_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询平台货品
     *
     * @desc 获取ERP的采购单信息 本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_api_goods_query.php
     */
    public function vipApiGoodsQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_api_goods_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_api_goods_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询组合装
     *
     * @desc 获取ERP中组合装界面的组合装资料
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=suites_query.php
     */
    public function suitesQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/suites_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/suites_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询货品品牌
     *
     * @desc 批量获取ERP内货品品牌档案信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=goods_brand_query.php
     */
    public function goodsBrandQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/goods_brand_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/goods_brand_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }
}
