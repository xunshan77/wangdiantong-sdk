<?php

namespace wangdian\sdk;

use wangdian\base\BaseApi;
use wangdian\exception\InvalidConfigException;
use wangdian\exception\InvalidFileSystemException;
use wangdian\exception\InvalidResponseException;

/**
 * Class Stock
 * 库存类
 *
 * @package wangdian\sdk
 */
class Stock extends BaseApi
{
    /**
     * 增量查询库存
     *
     * @desc 增量获取ERP实际库存、库存占用等数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_query.php
     */
    public function stockQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建盘点单
     *
     * @desc ERP库存需要调整时，推送盘点库存单据给ERP，覆盖ERP实物库存。注：ERP盘点成功后，盘点单内的库存值直接覆盖前库存
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_sync_by_pd.php
     */
    public function stockSyncByPd($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_sync_by_pd.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_sync_by_pd.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询盘点单
     *
     * @desc 获取调整ERP库存的盘点单据信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_pd_order_query.php
     */
    public function stockPdOrderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_pd_order_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_pd_order_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建其他入库单
     *
     * @desc ERP需要增加库存且入库单据没有对应的业务类型，调用本接口在ERP创建其他入库单，增加库存。
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_order_push.php
     */
    public function stockinOrderPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_order_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_order_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询入库单管理
     *
     * @desc 获取ERP的各种业务类型的入库单就其明细信息
     * 源单据类别        order_type        int        4        否        源单据类别
     *      1采购入库, 2调拨入库,  4盘盈入库, 5生产入库,
     *      6其他入库, 7保修入库, 8纠错入库, 9初始化入库 10 预入库 11 JIT退货入库 12 委外入库
     * 入库单状态        status        int        4        否        入库单状态
     *      10已取消20编辑中25待价格确认30待审核
     *      32待推送33推送失败35委外待入库60待结算80已完成（按照状态查询时必须传原单据类别，如果未传status则默认查询80已完成单据）
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_order_query.php
     */
    public function stockinOrderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_order_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_order_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建其他出库单
     *
     * @desc ERP需要减少库存且出库单据没有对应的业务类型，推送其他出库单给ERP，并执行出库操作（扣减库存等）
     * 在ERP中属于其他出库，order_type类型记录在单据的remark备注信息中。
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockout_order_push.php
     */
    public function stockoutOrderPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockout_order_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockout_order_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询出库单管理
     *
     * @desc 获取ERP各种业务类型的出库单及货品明细信息
     *  源单据类别      order_type    int    4    否
     * 出库单类型      order_type    tinyint    1    否
     *      2调拨出库,3采购退货出库,4盘亏出库,5生产出库,
     *      7其他出库,8多发出库,9纠错出库,10保修配件出库,
     *      11初始化出库，12jit拣货出库，13委外出库
     * 出库单状态    status    tinyint    1    否    出库单状态
     *      5：已取消，50：待审核，55：已审核，95：已发货，
     *      110：已完成（默认查已发货已完成单据）,113：异常发货
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockout_order_query.php
     */
    public function stockoutOrderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockout_order_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockout_order_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建调拨单
     *
     * @desc ERP内仓与仓之间的库存需要调度时，推送调拨单给ERP。
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_transfer_push.php
     */
    public function stockTransferPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_transfer_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_transfer_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建调拨出库单
     *
     * @desc 调拨业务走到出库步骤时，推送调拨出库单给ERP，执行调拨业务中的出库步骤
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockout_transfer_push.php
     */
    public function stockoutTransferPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockout_transfer_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockout_transfer_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建调拨入库单
     *
     * @desc ERP调拨业务中发货仓库出库完成，收货仓库需要入库单前推送调拨入库单给ERP
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_transfer_push.php
     */
    public function stockinTransferPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_transfer_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_transfer_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询调拨单
     *
     * @desc 获取ERP的调拨及货品明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_transfer_query.php
     */
    public function stockTransferQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_transfer_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_transfer_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 全量查询库存
     *
     * @desc 全量获取ERP实际库存、库存占用等数据
     * 注：①只允许凌晨00:00-02:00调用，其余时间调用会报错。
     * ②接口返回的“库存量”字段值是仓库中的实物库存量，“可发库存量”可理解为可销售库存量或未被占用库存量，可发库存计算方式，需要对照系统设置中的可发库存计算公式
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_stock_query_all.php
     */
    public function vipStockQueryAll($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_stock_query_all.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_stock_query_all.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建委外出入库单
     *
     * @desc 创建委外出入库单据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_wms_stockinout_order_push.php
     */
    public function vipWmsStockinoutOrderPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_wms_stockinout_order_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_wms_stockinout_order_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询委外出入库单
     *
     * @desc 获取委外出入库单及货品明细数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_stock_outside_wms_query.php
     */
    public function vipStockOutsideWmsQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_stock_outside_wms_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_stock_outside_wms_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询JIT退货入库单
     *
     * @desc 获取JIT退货入库单及其货品明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_jit_return_stockin_order_query.php
     */
    public function vipJitReturnStockinOrderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_jit_return_stockin_order_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_jit_return_stockin_order_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询JIT出库单
     *
     * @desc 获取JIT出库单及其货品明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_jit_stockout_order_query.php
     */
    public function vipJitStockoutOrderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_jit_stockout_order_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_jit_stockout_order_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询盘点盈亏统计
     *
     * @desc 获取盘点盈亏统计数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stat_stock_pd_detail_query.php
     */
    public function statStockPdDetailQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stat_stock_pd_detail_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stat_stock_pd_detail_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 序列号出入库查询
     *
     * @desc 序列号出入库查询
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_inout_sn_query.php
     */
    public function stockInoutSnQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_inout_sn_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_inout_sn_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 序列号管理查询接口
     *
     * @desc 获取序列号管理界面上的序列号信息
     * 注：（如果传序列号和商家编码时可以不用穿时间参数）
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stock_goods_sn_query.php
     */
    public function stockGoodsSnQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stock_goods_sn_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stock_goods_sn_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询店铺锁定库存
     *
     * @desc 获取ERP的店铺锁定库存信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=shop_stock_query.php
     */
    public function shopStockQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/shop_stock_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/shop_stock_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }
}
