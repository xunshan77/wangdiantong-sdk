<?php

namespace wangdian\sdk;

use wangdian\base\BaseApi;
use wangdian\exception\InvalidConfigException;
use wangdian\exception\InvalidFileSystemException;
use wangdian\exception\InvalidResponseException;

/**
 * Class Base
 * 基础类
 *
 * @package wangdian\sdk
 */
class Base extends BaseApi
{
    /**
     * 查询店铺
     *
     * @desc 获取ERP的店铺档案资料
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=shop.php
     */
    public function shop($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/shop.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/shop.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询仓库
     *
     * @desc 获取ERP的仓库档案资料
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=warehouse_query.php
     */
    public function warehouseQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/warehouse_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/warehouse_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询物流
     *
     * @desc 批量获取ERP内物流档案信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=logistics.php
     */
    public function logistics($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/logistics.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/logistics.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询供应商
     *
     * @desc 批量获取ERP内供应商档案信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_provider_query.php
     */
    public function purchaseProviderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/shop.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/shop.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建供应商
     *
     * @desc 创建供应商
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_provider_create.php
     */
    public function purchaseProviderCreate($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_provider_create.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_provider_create.php";
        }
        return $this->httpPostForJson($url, $data);
    }
}
