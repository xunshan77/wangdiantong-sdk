<?php

namespace wangdian\sdk;

use wangdian\base\BaseApi;
use wangdian\exception\InvalidConfigException;
use wangdian\exception\InvalidFileSystemException;
use wangdian\exception\InvalidResponseException;

/**
 * Class Order
 * 订单类
 *
 * @package wangdian\sdk
 */
class Order extends BaseApi
{
    /**
     * 创建原始订单
     *
     * @desc ①推送销售订单给ERP ；②更新已推送成功的销售订单。
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=trade_push.php
     */
    public function tradePush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/trade_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/trade_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询订单管理
     *
     * @desc 获取ERP销售订单信息 本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=trade_query.php
     */
    public function tradeQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/trade_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/trade_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询销售出库单
     *
     * @desc 获取ERP销售订单的出库单信息 本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockout_order_query_trade.php
     */
    public function stockoutOrderQueryTrade($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockout_order_query_trade.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockout_order_query_trade.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询物流同步
     *
     * @desc ERP销售订单的发货状态、物流单号等同步给其他系统，
     * 注：”查询物流同步”与“物流同步回写”两个接口配合使用，完成“销售订单发货同步”
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=logistics_sync_query.php
     */
    public function logisticsSyncQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/logistics_sync_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/logistics_sync_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 物流同步回写
     *
     * @desc 将物流同步（发货状态、物流单号等）是否成功的结果批量回传给ERP。
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=logistics_sync_ack.php
     */
    public function logisticsSyncAck($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/logistics_sync_ack.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/logistics_sync_ack.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询库存同步
     *
     * @desc 获取变化后的ERP可销库存，并同步至平台店铺
     * 注：”查询同步库存”与“库存同步回写”两个接口配合使用，完成“库存同步”
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=api_goods_stock_change_query.php
     */
    public function apiGoodsStockChangeQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/api_goods_stock_change_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/api_goods_stock_change_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 库存同步回写
     *
     * @desc 库存量同步至平台是否成功的状态批量回传给ERP
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=api_goods_stock_change_ack.php
     */
    public function apiGoodsStockChangeAck($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/api_goods_stock_change_ack.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/api_goods_stock_change_ack.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询销售汇总
     *
     * @desc 获取销售订单出库单汇总数据，查询EREP统计→销售→单品销售汇总表的“发货量”。 本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_stat_sales_by_spec_shop_warehouse_query.php
     */
    public function vipStatSalesBySpecShopWarehouseQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_stat_sales_by_spec_shop_warehouse_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_stat_sales_by_spec_shop_warehouse_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 重量回传
     *
     * @desc 将重量回传写入ERP销售订单出库单
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_stockout_sales_weight_push.php
     */
    public function vipStockoutSalesWeightPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_stockout_sales_weight_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_stockout_sales_weight_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 修改订单标记
     *
     * @desc 修改erp内订单的订单标记
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_trade_modify.php
     */
    public function vipTradeModify($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_trade_modify.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_trade_modify.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询原始订单
     *
     * @desc 查询电商平台的销售订单数据 本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_api_trade_query.php
     */
    public function vipApiTradeQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_api_trade_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_api_trade_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询平台账单
     *
     * @desc 获取ERP的平台账单信息 本接口不返回淘系订单数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=fa_api_account_detail_query.php
     */
    public function faApiAccountDetailQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/fa_api_account_detail_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/fa_api_account_detail_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询开票数据
     *
     * @desc 获取税控等开发票系统需要的开票数据
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_invoice_info_query.php
     */
    public function vipInvoiceInfoQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_invoice_info_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_invoice_info_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 回传开发票结果
     *
     * @desc 回传开发票结果信息发票号码、代码、电子发票的下载地址等
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=vip_invoice_info_update.php
     */
    public function vip_invoice_info_update($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/vip_invoice_info_update.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/vip_invoice_info_update.php";
        }
        return $this->httpPostForJson($url, $data);
    }
}
