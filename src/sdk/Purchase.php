<?php

namespace wangdian\sdk;

use wangdian\base\BaseApi;
use wangdian\exception\InvalidConfigException;
use wangdian\exception\InvalidFileSystemException;
use wangdian\exception\InvalidResponseException;

/**
 * Class Purchase
 * 采购类
 *
 * @package wangdian\sdk
 */
class Purchase extends BaseApi
{
    /**
     * 创建采购单
     *
     * @desc 逐个推送采购单据给ERP
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_order_push.php
     */
    public function purchaseOrderPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_order_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_order_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建采购入库单
     *
     * @desc 推送采购单对应的入库单给ERP。
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_purchase_push.php
     */
    public function stockinPurchasePush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_purchase_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_purchase_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询采购单
     *
     * @desc 获取ERP的采购单及其明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_order_query.php
     */
    public function purchaseOrderQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_order_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_order_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询采购入库单
     *
     * @desc 批量获取ERP采购单对应的入库单信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockin_order_query_purchase.php
     */
    public function stockinOrderQueryPurchase($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockin_order_query_purchase.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockin_order_query_purchase.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建采购退货单
     *
     * @desc 推送采购退货单据给ERP
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_return_push.php
     */
    public function purchaseReturnPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_return_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_return_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建采购退货出库单
     *
     * @desc 单个推送采购退货单对应的出库单给ERP
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_return_order_push.php
     */
    public function purchaseReturnOrderPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_return_order_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_return_order_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询采购退货单
     *
     * @desc 批量获取ERP的采购退货单及明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_return_query.php
     */
    public function purchaseReturnQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_return_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_return_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询采购退货出库单
     *
     * @desc 获取ERP的采购退货出库单及其明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=stockout_order_query_return.php
     */
    public function stockoutOrderQueryReturn($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/stockout_order_query_return.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/stockout_order_query_return.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 查询采购申请单
     *
     * @desc 获取ERP的采购申请单及其明细信息
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_apply_query.php
     */
    public function purchaseApplyQuery($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_apply_query.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_apply_query.php";
        }
        return $this->httpPostForJson($url, $data);
    }

    /**
     * 创建采购申请单
     *
     * @desc 逐个推送采购申请单据给ERP
     * @param array $data
     * @return array
     * @throws InvalidConfigException
     * @throws InvalidResponseException
     * @throws InvalidFileSystemException
     * @link https://open.wangdian.cn/qyb/open/apidoc/doc?path=purchase_apply_push.php
     */
    public function purchaseApplyPush($data)
    {
        if ($this->instanceDebug) {
            $url = "https://sandbox.wangdian.cn/openapi2/purchase_apply_push.php";
        } else {
            $url = "https://api.wangdian.cn/openapi2/purchase_apply_push.php";
        }
        return $this->httpPostForJson($url, $data);
    }
}
